package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import net.sf.json.JSONObject;

public class Proxy {
	public Proxy() {
	
	}
	/**
	 * installa ed usa una rete per le misurazioni. I parametri vengono
	 * passati tramite oggetto JSON. Ritorna una stringa che rappresenta
	 * l'URL a cui scaricare il file contenente i risultati.
	 * @param jsonObj
	 * @return String
	 * @throws IOException 
	 */
	public String installNetwork(JSONObject jsonObj) throws IOException{
		String platform, security, samplePeriod, sampleDuration, measurementCode;
		platform=jsonObj.getString("platform");
		security=jsonObj.getString("security");
		samplePeriod=jsonObj.getString("samplePeriod");
		sampleDuration=jsonObj.getString("sampleDuration");
		measurementCode=jsonObj.getString("measurementCode");
		//connessione		
		URL url = new URL("http://localhost:8080/ApplicationServer/MyHttpServlet?platform="+platform+
				"&security="+security+"&samplePeriod="+samplePeriod+"&sampleDuration="+sampleDuration
				+"&measurementCode="+measurementCode);			
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		return reader.readLine();	
	}	
}
